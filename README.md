# Skinny Widgets Form


form element

```
npm i sk-form sk-form-jquery --save
```

Quasi-form element that sends it's fields as FormData (multipart request), 

attributes are the same as for form element type

to have data submission working sk-form must have sk-button with type="submit" inside

supported form element types: 

**sk-input, sk-checkbox, sk-select, sk-datepicker, input, textarea**

if **novalidate** attribute is not set validation is performed on submit

if validation is not passed **forminvalid** event is emitted with info in **event.detail.errors**

if validation successfull **formvalid** event is emitted and form attempts to send data

it throws **formsubmitsuccess** event on 200 or any success response

and **formsubmiterror**

request/response data is stored in **event.detail.request** callback argument field

```html
<sk-form action="/submit" method="POST" id="myForm">
    <span slot="fields">
        <sk-input name="firstName" id="formInput1">
            <span slot="label">First Name</span>
        </sk-input>
        <sk-input name="secondName" id="formInput2">
            <span slot="label">Second Name</span>
        </sk-input>
        <sk-input name="address" id="formInput3">
            <span slot="label">Address</span>
        </sk-input>
        <sk-checkbox name="isSingle" id="formCheckbox"><span slot="label">Single</span></sk-checkbox>
        <sk-select name="gender" id="formSelect">
            <option value="male">Male</option>
            <option value="female">Female</option>
        </sk-select>
        <sk-button id="formButton" type="submit" button-type="primary">
            <span slot="label">Submit</span>
        </sk-button>
    </span>
</sk-form>


<script type="module">
    import { SkButton } from '/node_modules/sk-button/src/sk-button.js';
    import { SkInput } from '/node_modules/sk-input/src/sk-input.js';
    import { SkSelect } from '/node_modules/sk-select/src/mysk-select.js';
    import { SkForm } from '/node_modules/sk-form/src/sk-form.js';
    import { SkCheckbox } from '/node_modules/sk-checkbox/src/sk-checkbox.js';

    customElements.define('sk-button', SkButton);
    customElements.define('sk-input', SkInput);
    customElements.define('sk-select', SkSelect);
    customElements.define('sk-form', SkForm);
    customElements.define('sk-checkbox', SkCheckbox);

    myForm.addEventListener('formsubmitsuccess', (event) => {
        console.log('form submit success handled', event);
    });
    myForm.addEventListener('formsubmiterror', (event) => {
        console.log('form submit error handled', event);
    });
</script>
```

### Action buttons

while bootstrap process **sk-form** lookups for all internal **button** and **sk-button** elements with and bind 
them with callback by name or as firing events (see similar sk-dialog functionality for examples). So you can extend
form with your own button triggered logic.

### Field Validation

You can bind custom validation function for every field:

```html
<script>
    function validateFirstName(el) {
        if (el.value == '1') {
            return true;
        } else {
            return 'Wrong value';
        }
    }
</script>
<sk-input name="firstName" id="formInput1" form-validation="validateFirstName" form-validation-msg="{'someError': 'This field has some error'}">First Name</sk-input>
```

**form-validation** attribute specifies validation function name, it can be binded to global (window) or field class.
Validation function returns error message or validation key to load from **form-validation-msg** or true in case value is valid.

To enable validation labels display use **validation-label** attribute of an element or **sk-form**, use value **"disabled"** to 
force it's switch off.

**no-live-validation** attribute disables field revalidation on **input**

### Backend validation

You can use **response-validation** sk-form attribute or define **'validateResponse'** method to provide post-submit validator 
for cases when backend can return validation result json. Xml Http Request Event will be passed as argument of this method and 
**formsubmitsuccess** or **formsubmiterror** events will be triggered depending on validator function execution result. 
As it is called with form context you can use form methods or resources to indicate error.

```html
<sk-config
    theme="jquery"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-jquery"
    lang="ru"
    id="myConfig"
></sk-config>


<script type="module">
    import { SkCheckbox } from './node_modules/sk-checkbox/src/sk-checkbox.js';
    import { SkButton } from './node_modules/sk-button/src/sk-button.js';
    import { SkConfig } from './node_modules/sk-config/src/sk-config.js';
    import { SkSelect } from './node_modules/sk-select/src/sk-select.js';
    import { SkInput } from './node_modules/sk-input/src/sk-input.js';
    import { SkForm } from './node_modules/sk-form/src/form/sk-form.js';

    customElements.define('sk-config', SkConfig);
    customElements.define('sk-checkbox', SkCheckbox);
    customElements.define('sk-button', SkButton);
    customElements.define('sk-select', SkSelect);
    customElements.define('sk-input', SkInput);
    customElements.define('sk-form', SkForm);


    myForm.addEventListener('formsubmitsuccess', (event) => {
        console.log('form submit success handled', event);
    });
    myForm.addEventListener('formsubmiterror', (event) => {
        console.log('form submit success handled', event);
    });
    myForm.responseValidator = function(event) {
        let errors = event.response.fieldErrors;
        if (errors) {
            for (let fieldName of Object.keys(errors)) {
                let field = this.querySelector(`[name='${fieldName}']`);
                if (field) {
                    let message = '';
                    if (Array.isArray(errors[fieldName])) {
                        message = errors[fieldName].join(' ');
                    } else {
                        message = errors[fieldName];
                    }
                    field.setCustomValidity(message);
                    this.impl.renderFieldInvalid(field);
                }
            }
        }
        // remove error indication from valid fields
        let fields = this.impl.queryFields();
        for (let field of fields) {
            let fieldName = field.getAttribute('name');
            if (fieldName && ! errors[fieldName]) {
                this.impl.renderFieldValid(field);
            }
        }
    };
</script>

<sk-form id="myForm" action="/submit" method="POST" response-validation="responseValidator" validation-label>

        <sk-select id="skSelect2" name="skSelect2" multiple>
            <option value="apple">Apple</option>
            <option value="banana">Banana</option>
            <option value="lemon">Lemon</option>
        </sk-select>
        <sk-checkbox id="myCheckbox" name="myCheckbox">Checkbox</sk-checkbox>
        <sk-input id="myInput" name="myInput">My Input</sk-input>

    <sk-button id="formButton" type="submit" button-type="primary">Submit</sk-button>
</sk-form>

```

where response is like:

```json
{
  "fieldErrors": {
    "myInput": [
      "Hey, your input is invalid !"
    ]
  },
  "result": "error"
}
```

#### template

id: SkFormTpl
