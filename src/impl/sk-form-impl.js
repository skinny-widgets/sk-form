

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';
import { XmlHttpClient } from '../../../sk-core/complets/http/xml-http-client.js';

import { SkRequired } from "../../../sk-form-validator/src/validators/sk-required.js";
import { SkMax } from "../../../sk-form-validator/src/validators/sk-max.js";
import { SkEmail } from "../../../sk-form-validator/src/validators/sk-email.js";
import { SkPattern } from "../../../sk-form-validator/src/validators/sk-pattern.js";
import { SkMin } from "../../../sk-form-validator/src/validators/sk-min.js";

export var FORM_SUBELEMENTS_SL = 'sk-input,sk-checkbox,sk-select,sk-datepicker,sk-switch,input,textarea,select';

export class SkFormImpl extends SkComponentImpl {

    get formEl() {
        if (! this._formEl) {
            this._formEl = this.comp.el.querySelector('form');
        }
        return this._formEl;
    }

    get formSubElementsSl() {
        if (! this._formSubElementsSl) {
            this._formSubElementsSl = FORM_SUBELEMENTS_SL;
        }
        return this._formSubElementsSl;
    }

    set formSubElementsSl(sl) {
        this._formSubElementsSl = sl;
    }

    bindEvents() {
        super.bindEvents();
        if (this.submitHandler) {
            this.comp.removeEventListener('formsubmit', this.submitHandler);
        }
        this.submitHandler = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onSubmit(event);
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.comp.addEventListener('formsubmit', this.submitHandler);
        this.onSuccessHandler = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onSubmitSuccess(event);
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.onErrorHandler = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onSubmitError(event);
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.onValid = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onFormValid(event);
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.onInvalid = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onFormInvalid(event);
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.validators = {};
        if (! this.comp.hasAttribute('no-live-validation')) {
            this.logger.debug('binding form custom validators');
            this.bindFieldValidators();
        }
        this.bindActions(this.comp);
    }

    unbindEvents() {
        super.unbindEvents();
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState();
        this.applyValidationLabel();
    }

    applyValidationLabel() {
        if (this.comp.hasAttribute('validation-label')) {
            let validationLabel = this.comp.getAttribute('validation-label');
            let fields = this.comp.el.host.querySelectorAll(this.formSubElementsSl);
            for (let field of fields) {
                if (!field.hasAttribute('validation-label')) {
                    field.setAttribute('validation-label', validationLabel);
                }
            }
        }
    }

    get httpClient() {
        if (! this._http) {
            this._http = new XmlHttpClient();
        }
        return this._http;
    }

    onSubmitSuccess(event) {
        let validationResult = false;
        if (this.comp.hasAttribute('response-validation')) {
            let validator = this.comp.getAttribute('response-validation');
            if (validator === 'false' || validator === 'no' || validator === 'disabled') {
                validationResult = true;
            } else {
                if (typeof this.comp[validator] === 'function') {
                    validationResult = this.comp[validator].call(this.comp, event);
                } else if (typeof this[validator] === 'function') {
                    validationResult = this[validator].call(this, event);
                } else if (typeof window[validator] === 'function') {
                    validationResult = window[validator].call(this.comp, event);
                } else if (typeof this.comp['validateResponse'] === 'function') {
                    validationResult = this.comp['validateResponse'].call(this.comp, event);
                } else if (typeof this['validateResponse'] === 'function') {
                    validationResult = this['validateResponse'].call(this.comp, event);
                } else {
                    this.logger.warn(`Specified validator ${validator} not found`);
                    validationResult = true;
                }
            }
        } else {
            validationResult = true;
        }
        if (validationResult) {
            this.comp.dispatchEvent(new CustomEvent('formsubmitsuccess',
                {bubbles: true, composite: true, detail: {request: event}})
            );
            this.comp.dispatchEvent(new CustomEvent('skformsubmitsuccess',
                {bubbles: true, composite: true, detail: {request: event}})
            );
        } else {
            this.onSubmitError(event);
        }
    }

    onFormValid() {
        this.comp.dispatchEvent(new CustomEvent('formvalid',
            { bubbles: true, composite: true, detail: {  }})
        );
        this.comp.dispatchEvent(new CustomEvent('skformvalid',
            { bubbles: true, composite: true, detail: {  }})
        );
    }

    onFormInvalid(errors) {
        this.comp.dispatchEvent(new CustomEvent('forminvalid',
            { bubbles: true, composite: true, detail: { errors: errors }})
        );
        this.comp.dispatchEvent(new CustomEvent('skforminvalid',
            { bubbles: true, composite: true, detail: { errors: errors }})
        );
    }

    onSubmitError(event) {
        this.comp.dispatchEvent(new CustomEvent('formsubmiterror',
            { bubbles: true, composite: true, detail: { request: event }})
        );
        this.comp.dispatchEvent(new CustomEvent('skformsubmiterror',
            { bubbles: true, composite: true, detail: { request: event }})
        );
    }

    onFieldChange(event) {
        this.validateCustom(event.target);
    }

    bindFieldValidators() {
        let fields = this.queryFields();
        for (let field of fields) {
            let name = field.getAttribute('name'); // :TODO generate identifier if no name provided
            this.bindFieldValidation(name, field);
        }
    }

    onDisconnected() {
        this.unbindCustomValidators();
    }

    unbindCustomValidators() {
        let fields = this.queryFields();
        for (let field of fields) {
            let name = field.getAttribute('name'); // :TODO generate identifier if no name provided
            field.oninput = null;
            field.onchange = null;
        }
    }

    onSubmit(event) {
        let fields = this.queryFields();
        let formData = new FormData();
        if (!this.validationDisabled()) {
            let errors = {};
            for (let field of fields) {
                let name = field.getAttribute('name'); // :TODO generate identifier if no name provided
                if (field.validity.customError) {
                    field.setCustomValidity('');
                }
                let validatedField = this.validateBuildIns(field);
                validatedField = this.validateCustom(field);
                if (! this.comp.hasAttribute('no-live-validation')) {
                    this.bindFieldValidation(name, field);
                }
                if (validatedField.validity.valid) {
                    this.addFieldValue(validatedField, name, formData);
                    if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
                        this.flushFieldValidity(validatedField);
                    }
                    this.renderFieldValid(validatedField);
                } else {
                    if (! validatedField.hasAttribute('form-validation')) {
                        this.renderFieldInvalid(validatedField);
                    }
                    if (this.comp.getAttribute('validation-label') !== 'disabled'
                        && field.getAttribute('validation-label') !== 'disabled') {
                        if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
                            this.flushFieldValidity(field);
                            this.renderFieldValidation(field);
                        }
                    }
                    errors[name] = validatedField.validity;
                }
            }
            if (Object.keys(errors).length === 0) {
                this.onFormValid();
                this.sendFormData(formData);
            } else {
                this.onFormInvalid(errors);
            }
        } else {
            for (let field of fields) {
                let name = field.getAttribute('name');
                this.addFieldValue(field, name, formData);
            }
            this.sendFormData(formData);
        }
    }

    queryFields() {
        let fields = this.comp.el.host.querySelectorAll(this.formSubElementsSl);
        return fields;
    }

    bindFieldValidation(name, field) {
        if (!this.validators[name]) {
            this.validators[name] = this.onFieldChange.bind(this);
            field.oninput = field.onchange = function(event) {
                this.validators[name](event);
            }.bind(this);
            field.addEventListener('formchange', this.validators[name]);
        }
    }

    validationDisabled() {
        return this.comp.getAttribute('novalidate') && this.comp.getAttribute('novalidate') !== '';
    }

    get skValidators() {
        if (! this._skValidators) {
            this._skValidators = {
                'sk-required': new SkRequired(),
                'sk-min': new SkMin(),
                'sk-max': new SkMax(),
                'sk-email': new SkEmail(),
                'sk-pattern': new SkPattern(),
            };
        }
        return this._skValidators;
    }

    validateBuildIns(field) {
        let result = false;
        for (let validatorAttr of Object.keys(this.skValidators)) {
            if (field.hasAttribute(validatorAttr)) {
                result = this.skValidators[validatorAttr].validate(field, this);
                if (typeof result === 'string') {
                    field.setCustomValidity(this.comp.locale.tr(result));
                }
            }
        }
    }

    validateCustom(field) {
        let formValidation = field.getAttribute('form-validation');
        let validationMessages = {};
        try {
            if (field.hasAttribute('form-validation-msg')) {
                validationMessages = JSON.parse(field.getAttribute('form-validation-msg'));
            }
        } catch {
            this.logger.debug('failed to load validation texts from attribute for field', field);
        }
        if (formValidation) {
            let validationMsgKey = null;
            if (typeof field[formValidation] === 'function') {
                validationMsgKey = field[formValidation].call(this, field, this);
            } else if (typeof window[formValidation] === 'function') {
                validationMsgKey = window[formValidation].call(window, field, this);
            }
            if (typeof field['setCustomValidity'] === 'function') {
                if (validationMsgKey !== true) {
                    if (validationMessages[validationMsgKey]) {
                        validationMsgKey = validationMessages[validationMsgKey];
                    }
                    field.setCustomValidity(typeof validationMsgKey === 'string'
                        ? validationMsgKey : this.comp.locale.tr('Field value is invalid'));
                    if (this.comp.getAttribute('validation-label') !== 'disabled'
                        && field.getAttribute('validation-label') !== 'disabled') {
                        if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
                            this.flushFieldValidity(field);
                            this.renderFieldValidation(field);
                        }
                    }
                    this.renderFieldInvalid(field);
                } else {
                    if (typeof field['flushValidity'] === 'function') {
                        field.flushValidity();
                    } else { // possibly native element
                        field.setCustomValidity('');
                        if (this.comp.getAttribute('validation-label') !== 'disabled'
                            && field.getAttribute('validation-label') !== 'disabled') {
                            this.flushFieldValidity(field);
                        }
                    }
                    this.renderFieldValid(field);
                }
            }
        }
        return field;
    }

    renderFieldValidation(field) {
        let msgEl = field.nextElementSibling;
        let msgExists = false;
        if (! msgEl) {
            msgEl = this.comp.renderer.createEl('span');
            msgEl.classList.add('form-validation-message');
        } else {
            msgExists = true;
            msgEl.innerHTML = '';
        }
        msgEl.insertAdjacentHTML('afterbegin', field.validationMessage);
        if (! msgExists) {
            field.insertAdjacentHTML('afterend', msgEl.outerHTML);
        }
    }

    flushFieldValidity(field) {
        let possibleLabel = field.nextElementSibling;
        if (possibleLabel && possibleLabel.classList.contains('form-validation-message')) {
            possibleLabel.parentElement.removeChild(possibleLabel);
        }
    }

    renderFieldInvalid(field) {
        if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
            field.classList.add('form-invalid');
        } else {
            field.setAttribute('form-invalid', '');
        }
    }

    renderFieldValid(field) {
        if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
            field.classList.remove('form-invalid');
        } else {
            field.removeAttribute('form-invalid');
        }
    }

    addFieldValue(field, name, formData) {
        let value = null;
        if (field.type === 'checkbox' || field.type === 'radio') {
            if (field.getAttribute('checked')) {
                value = field.value ? field.value : '1';
            }
        } else if ((field.type === 'select' || field.tagName === 'SELECT') && ! field.value) {
            value = field.selectedOptions[0].value;
        } else {
            value = field.value || field.getAttribute('value');
        }
        if (name && value) {
            formData.append(name, value);
        }
    }

    sendFormData(formData) {
        let action = this.comp.getAttribute('action');
        let method = this.comp.getAttribute('method');
        let enctype = this.comp.getAttribute('enctype') || 'application/x-www-form-urlencoded';

        if (enctype === 'application/json') {
            if (Object.fromEntries) {
                formData = JSON.stringify(Object.fromEntries(formData));
            } else {
                formData = JSON.stringify(this.comp.fromEntries(formData));
            }
        }
        let req = this.httpClient.request(action, method, enctype, formData);
        req.then(this.onSuccessHandler, this.onErrorHandler);
    }
}