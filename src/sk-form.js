
import { SkElement } from '../../sk-core/src/sk-element.js';


export class SkForm extends SkElement {

    get cnSuffix() {
        return 'form';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    reset() {
        let fields = this.impl.queryFields();
        for (let field of fields) {
            if (typeof field.reset === 'function') {
                field.reset();
            } else if (field.tagName === 'INPUT' || field.tagName === 'TEXTAREA') {
                field.value = '';
            } else if (field.tagName === 'SK-CHECKBOX' || field.tagName === 'SK-SWITCH' || field.tagName === 'CHECKBOX') {
                field.removeAttribute('checked');
            } else if (field.hasAttribute('value') || field.tagName === 'SK-INPUT'
                            || field.tagName === 'SK-DATEPICKER' || field.tagName === 'SK-SELECT') {
                field.setAttribute('value', '');
            } else {
                field.value = '';
            }
        }
    }

    focus() {
        setTimeout(() => {
            this.input.formEl.focus();
        }, 0);
    }
}
